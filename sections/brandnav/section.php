<?php
/*
	Section: BrandNav
	Author: PageLines
	Author URI: http://www.pagelines.com
	Description: Branding and Nav Inline
	Class Name: PageLinesBrandNav
	Depends: PageLinesNav
	Workswith: header
*/

/**
 * BrandNav Section
 *
 * @package PageLines Framework
 * @author PageLines
 */
class PageLinesBrandNav extends PageLinesNav {

	/**
	* PHP that always loads no matter if section is added or not.
	*/	
	function section_persistent(){
			register_nav_menus( array( 'brandnav' => __( 'BrandNav Section Navigation', 'pagelines' ) ) );
	}
	
	/**
	*
	* @TODO document
	*
	*/
	function section_styles(){
		if(ploption('enable_drop_down')){
			
			wp_register_style('superfish', self::$nav_url . '/style.superfish.css', array(), CORE_VERSION, 'screen');
		 	wp_enqueue_style( 'superfish' );
		
			wp_enqueue_script( 'superfish', self::$nav_url . '/script.superfish.js', array('jquery'), '1.4.8', true );
			wp_enqueue_script( 'bgiframe', self::$nav_url . '/script.bgiframe.js', array('jquery'), '2.1', true );	
		}
	}

	/**
	* Section template.
	*/
 	function section_template() { 
	
			pagelines_main_logo( $this->id ); 
			
			
		if(has_action('brandnav_after_brand')){
			pagelines_register_hook( 'brandnav_after_brand', 'brandnav' ); // Hook
		
		} else {
		
		?>
		
			<div class="brandnav-nav main_nav fix">	
			
<?php 	
				wp_nav_menu( array('menu_class'  => 'main-nav tabbed-list'.pagelines_nav_classes(), 'container' => null, 'container_class' => '', 'depth' => 3, 'theme_location'=>'brandnav', 'fallback_cb'=>'pagelines_nav_fallback') );
				
				pagelines_register_hook( 'brandnav_after_nav', 'brandnav' ); // Hook
				
				printf('<div class="icons" style="bottom: %spx; right: %spx;">', intval(pagelines_option('icon_pos_bottom')), pagelines_option('icon_pos_right'));
					 
			
					if(ploption('rsslink'))
						printf('<a target="_blank" href="%s" class="rsslink"><img src="%s" alt="RSS"/></a>', apply_filters( 'pagelines_branding_rssurl', get_bloginfo('rss2_url') ), $this->base_url.'/rss.png' );
					
					if(VPRO) {
						if(ploption('twitterlink'))
							printf('<a target="_blank" href="%s" class="twitterlink"><img src="%s" alt="Twitter"/></a>', ploption('twitterlink'), $this->base_url.'/twitter.png');
					
						if(ploption('facebooklink'))
							printf('<a target="_blank" href="%s" class="facebooklink"><img src="%s" alt="Facebook"/></a>', ploption('facebooklink'), $this->base_url.'/facebook.png');
						
						if(ploption('linkedinlink'))
							printf('<a target="_blank" href="%s" class="linkedinlink"><img src="%s" alt="LinkedIn"/></a>', ploption('linkedinlink'), $this->base_url.'/linkedin.png');
						
						if(ploption('youtubelink'))
							printf('<a target="_blank" href="%s" class="youtubelink"><img src="%s" alt="Youtube"/></a>', ploption('youtubelink'), $this->base_url.'/youtube.png');
						
						if(ploption('gpluslink'))
							printf('<a target="_blank" href="%s" class="gpluslink"><img src="%s" alt="Google+"/></a>', ploption('gpluslink'), $this->base_url.'/google.png');
							}
?>
			
			</div>
		<div class="clear"></div>
<?php 	}
	}


		/**
		*
		* @TODO document
		*
		*/
		function section_head(){

			$arrows = (ploption('drop_down_arrows') == 'on') ? 1 : 0;
			$shadows = (ploption('drop_down_shadow') == 'on') ? 1 : 0;

			if(ploption('enable_drop_down')): ?>

	<script type="text/javascript"> /* <![CDATA[ */ jQuery(document).ready(function() {  jQuery('div.brandnav-nav ul.sf-menu').superfish({ delay: 100, speed: 'fast', autoArrows:  <?php echo $arrows;?>, dropShadows: <?php echo $shadows;?> });  }); /* ]]> */ </script>			

	<?php 
			endif;
	}
}
